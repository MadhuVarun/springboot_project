package com.example.drag.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/message")
public class DragController {
	@GetMapping
	public String call() {
		return "What a Drag";
	}

	@GetMapping("/test")
	public String requestParamCall(@RequestParam String name) {
		return "My Name is: " + name;

	}

	@GetMapping("/test/{id}")
	public String getdetailsById(@PathVariable("id") int sapId) {
		return "My SAPID is: " + sapId;

	}

	@PostMapping("/test/{id}")
	public String postdetails(@PathVariable("id") int sapId) {
		return "My SAPID is: " + sapId;

	}
}
