package com.example.drag;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DragApplication {

	public static void main(String[] args) {
		SpringApplication.run(DragApplication.class, args);
		
		System.out.println("Started");
	}

}
